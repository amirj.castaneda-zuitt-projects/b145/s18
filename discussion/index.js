function countUsingWhile() {
	let input1 = document.getElementById('task1').value;
	if (input1 <= 0) {
		let msg = document.getElementById('message');
		msg.innerHTML = 'Value not Valid'
	}
	else {
		while (input1 !== 0) {
			alert(input1);
			input1--;
		}
	}
}

function countUsingDoWhile() {
	let number = document.getElementById('task2').value;
	if (number <= 0) {
		let displayText = document.getElementById('info');
		displayText.innerHTML = 'The number is not valid.';
	}
	else {
		let indexStart = 1;
		let displayText = document.getElementById('info');
		displayText.innerHTML = `${number} is valid`;
		do {
		alert(indexStart);
		indexStart++;
	}   
		while (indexStart <= number);
		
	}
}

// Section for For Loops

function countUsingForLoops() {
	let userInput = document.getElementById('task3').value;
	let response = document.getElementById('response');

	if (userInput <= 0) {
		response.innerHTML = "Invalid Number";
	}
	else {
		for (let i=0; i  <= userInput; i++) {
			response.innerHTML = "Nice Number";
			alert(i);
		}
	}

}

function accessElementsInString() {
	let user = document.getElementById('userName').value;
	let textDisplay = document.getElementById('name-length');
	if (user !=='') {
		textDisplay.innerHTML = `The string is ${user.length} characters long.`;
		for (let i=0;  i < user.length; i++) {
			console.log(user[i]);
		}
	}
	else {
		alert("Invalid value")
	}
}


// // Solution 1
// function palindromeChecker() {
// 	let word = document.getElementById('word').value;
// 	let displayMessage = document.getElementById('detectPalindrome');

// 	if (word === '') {
// 		displayMessage.innerHTML = '<h3 class="text-danger"> Value is Invalid </h3>'
// 	}
// 	else {
// 		displayMessage.innerHTML = '<h3 class="text-success"> Value is Valid </h3>'
// 		let opposite='';
// 		for (let i=word.length-1; i > -1; i--) {
// 			opposite += word[i].toLowerCase();
// 		}
// 		alert(opposite==word ?"Palindrome!":"Not a Palindrome!")
// 	}
// }

// Solution 2

function palindromeChecker() 
{
	let word = document.getElementById('word').value;
	let displayMessage = document.getElementById('detectPalindrome');
	if (word !== '') {
		let wrdLength=word.length;
		for (let index=0; index < wrdLength/2; index++) {
			if (word[index] !== word[wrdLength-1-index]) {
				displayMessage.innerHTML = '<h3 class="text-danger"> Not a Palindrome! </h3>';
				break;
			}
			else {
				displayMessage.innerHTML = '<h3 class="text-success"> A Palindrome! </h3>';
			}
		}
	}
}

function getOddNumbers() {
	let inputCount =  document.getElementById('value4').value;
	let res = document.getElementById('getOddNum');
	if (inputCount > 0) {
		for (let count=0; count <= inputCount; count++) {
			if (count % 2 !== 0) {
				console.log(count);
			}
		}
	}
	else {
		res.innerHTML = '<h3 class="text-danger"> The Numbber should be greater than 0 </h3>';
	}
}

function vowelRemover(initialString) {
	let vowelList = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
	let newString = '';
	for (let i = 0; i < initialString.length; i++) {
		console.log(i);
		if (vowelList.includes(initialString[i]) == false) {
			newString += initialString[i];
		}
	}
	return newString;
}

function largestOfFour(arr) {
  let newArr = [];
  for (let i=0; i < arr.length; i++) {
    let largestNum = -Infinity;
    for (let j=0; j < arr[i].length; j++){
      if (arr[i][j] > largestNum) {
        largestNum = arr[i][j];
      }
    }
    newArr.push(largestNum);
  }
  console.log(newArr);
  return newArr;
};


let inputNum = 5;
function countToFifty(num) {
	for (let i=num; i <= 50; i++) {
		if (i % 10 !== 0) {
			console.log(i) }
	}
}
countToFifty(inputNum);
